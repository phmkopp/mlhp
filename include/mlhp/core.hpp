// This file is part of the mlhp project. License: See LICENSE

#ifndef MLHP_CORE_HPP
#define MLHP_CORE_HPP

#include "mlhp/core/algorithm.hpp"
#include "mlhp/core/alias.hpp"
#include "mlhp/core/arrayfunctions.hpp"
#include "mlhp/core/assembly.hpp"
#include "mlhp/core/basis.hpp"
#include "mlhp/core/basisevaluation.hpp"
#include "mlhp/core/boundary.hpp"
#include "mlhp/core/compilermacros.hpp"
#include "mlhp/core/dense.hpp"
#include "mlhp/core/derivativeHelper.hpp"
#include "mlhp/core/forwarddeclare.hpp"
#include "mlhp/core/implicit.hpp"
#include "mlhp/core/integrands.hpp"
#include "mlhp/core/integrandtypes.hpp"
#include "mlhp/core/kdtree.hpp"
#include "mlhp/core/logging.hpp"
#include "mlhp/core/mapping.hpp"
#include "mlhp/core/memory.hpp"
#include "mlhp/core/mesh.hpp"
#include "mlhp/core/multilevelhpcore.hpp"
#include "mlhp/core/ndarray.hpp"
#include "mlhp/core/numeric.hpp"
#include "mlhp/core/parallel.hpp"
#include "mlhp/core/partitioning.hpp"
#include "mlhp/core/polynomials.hpp"
#include "mlhp/core/postprocessing.hpp"
#include "mlhp/core/quadrature.hpp"
#include "mlhp/core/refinement.hpp"
#include "mlhp/core/sparse.hpp"
#include "mlhp/core/spatial.hpp"
#include "mlhp/core/topologycore.hpp"
#include "mlhp/core/triangulation.hpp"
#include "mlhp/core/utilities.hpp"

#endif // MLHP_CORE_HPP

