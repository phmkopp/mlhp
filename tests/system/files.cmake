# This file is part of the mlhp project. License: See LICENSE

set( MLHP_SYSTEM_TEST_SOURCES 
     elasticity_test.cpp
     j2plasticity_test.cpp 
     linear_heat.cpp 
     main_test.cpp 
     planestress_test.cpp
     singular_L2_test.cpp 
     singular_Poisson_test.cpp
)

set( MLHP_PYTHON_TESTS 
     __init__.py
     linear_heat_test.py
     pressurised_sphere_test.py
     singular_poisson_test.py
     singular_L2_test.py
)
